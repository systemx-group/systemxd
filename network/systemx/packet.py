import io
import os

class Packet(io.BytesIO):
    def __init__(self, id = None, data = None):
        super().__init__(data)

        if not data:
            self.set_length(0)
            self.set_id(id)

        self.seek(6)

    def get_length(self):
        old_pos = self.tell()
        self.seek(0, os.SEEK_SET)
        length = self.read_uint32()
        self.seek(old_pos)

        return length

    def set_length(self, length):
        old_pos = self.tell()
        self.seek(-old_pos, os.SEEK_END)
        self.write_uint32(length)
        self.seek(4)

    def get_id(self):
        old_pos = self.tell()
        self.seek(4, os.SEEK_SET)
        id = self.read_uint16()
        self.seek(old_pos)

        return id

    def set_id(self, id):
        old_pos = self.tell()
        self.seek(-old_pos + 4, os.SEEK_END)
        self.write_uint16(id)
        self.seek(6)

    def read_int8(self):
        return int.from_bytes(self.read(1), byteorder="big", signed=True)

    def write_int8(self, value):
        self.write(value.to_bytes(1, byteorder="big", signed=True))

    def read_uint8(self):
        return int.from_bytes(self.read(1), byteorder="big", signed=False)

    def write_uint8(self, value):
        self.write(value.to_bytes(1, byteorder="big", signed=False))

    def read_int16(self):
        return int.from_bytes(self.read(2), byteorder="big", signed=True)

    def write_int16(self, value):
        self.write(value.to_bytes(2, byteorder="big", signed=True))

    def read_uint16(self):
        return int.from_bytes(self.read(2), byteorder="big", signed=False)

    def write_uint16(self, value):
        self.write(value.to_bytes(2, byteorder="big", signed=False))

    def read_int32(self):
        return int.from_bytes(self.read(4), byteorder="big", signed=True)

    def write_int32(self, value):
        self.write(value.to_bytes(4, byteorder="big", signed=True))

    def read_uint32(self):
        return int.from_bytes(self.read(4), byteorder="big", signed=False)

    def write_uint32(self, value):
        self.write(value.to_bytes(4, byteorder="big", signed=False))

    def read_int64(self):
        return int.from_bytes(self.read(8), byteorder="big", signed=True)

    def write_int64(self, value):
        self.write(value.to_bytes(8, byteorder="big", signed=True))

    def read_uint64(self):
        return int.from_bytes(self.read(8), byteorder="big", signed=False)

    def write_uint64(self, value):
        self.write(value.to_bytes(8, byteorder="big", signed=False))

    def read_string(self):
        str_len = self.read_uint16()

        str = ""
        for i in range(str_len):
            str += chr(self.read_uint8())

        return str

    def write_string(self, value):
        self.write_uint16(len(value))

        for i in range(len(value)):
            self.write_uint8(ord(value[i]))

    def __bytes__(self):
        self.set_length(len(self.getvalue()) - 4)

        return self.getvalue()

    def from_bytes(data):
        return Packet(data = data)
