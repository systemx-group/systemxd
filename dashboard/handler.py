from http.server import BaseHTTPRequestHandler
import os
import mimetypes
from urllib.parse import urlparse, parse_qs
import re

class SystemXHTTPRequestHandler(BaseHTTPRequestHandler):
    def parse(self):
        self.path_parsed = urlparse(self.path)
        self.params = parse_qs(self.path_parsed.query)

    def parsed_path(self):
        return self.path_parsed.path

    def match(self, regex):
        regex = re.compile(regex)
        self.regex_match = regex.match(self.parsed_path())

        return self.regex_match

    def group(self, key):
        return self.regex_match.group(key)

    def do_file(self, path):
        file = open(path, "rb")
        self.send_header("Content-Type", mimetypes.guess_type(path)[0])
        self.send_header("Content-Length", os.stat(path).st_size)

        self.end_headers()

        self.wfile.write(file.read())
        file.close()

    def do_response_file(self):
        www_path = os.getcwd() + "/www"

        file_path = os.path.abspath(www_path + self.path)
        if file_path.startswith(www_path):
            self.parse()

            if self.match(r"^/js/plugins/(?P<plugin>[a-z]+).js$"):
                for plugin in os.listdir("plugins"):
                    plugin_path = os.path.join("plugins", plugin)
                    if plugin == "__pycache__" or not os.path.isdir(plugin_path):
                        continue

                    if plugin == self.group("plugin"):
                        self.send_response(200)

                        plugin_js_path = os.path.join(plugin_path, "%s.js" % (plugin,))

                        self.do_file(plugin_js_path)
                        return

                self.send_error(404)
            else:
                self.send_response(200)

                if not os.path.exists(file_path) or not os.path.isfile(file_path):
                    file_path = www_path + "/index.html"

                self.do_file(file_path)
        else:
            self.send_error(403)

    def do_GET(self):
        self.do_response_file()
