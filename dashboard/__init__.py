from http.server import HTTPServer
from dashboard.handler import SystemXHTTPRequestHandler

class SystemXHTTPServer(HTTPServer):
    def __init__(self, address):
        super().__init__(address, SystemXHTTPRequestHandler)
