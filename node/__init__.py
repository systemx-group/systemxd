from node.client import SystemXClient
from network.systemx.packet import Packet

class SystemXNode:
    systemx = None

    def initialize():
        SystemXNode.systemx = SystemXClient()

    def run():
        SystemXNode.systemx.connect(("localhost", 3000))
        for i in range(10000000):
            packet = Packet(255)
            packet.write_uint16(1)
            packet.write_uint16(5)
            packet.write_string("lol")
            SystemXNode.systemx.send(bytes(packet))

    def uninitialize():
        SystemXNode.systemx.close()
