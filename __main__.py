from node import SystemXNode
from manager import SystemXManager, SystemXManagerServer, SystemXAPIServer, SystemXServer
from manager.services import Services
from dashboard import SystemXHTTPServer
import sys
import threading

if "--node" in sys.argv:
    SystemXNode.initialize()
    try:
        SystemXNode.run()
    except KeyboardInterrupt:
        pass
    SystemXNode.uninitialize()
elif "--manager" in sys.argv:
    managerd = SystemXManagerServer(("localhost", 3333))
    apid = SystemXAPIServer(("localhost", 8080))
    systemxd = SystemXServer(("localhost", 3000))

    SystemXManager.initialize()

    def run_managerd():
        print("managerd listen...")
        managerd.serve_forever()

    def run_apid():
        print("apid listen...")
        apid.serve_forever()

    def run_systemxd():
        print("systemxd listen...")
        systemxd.serve_forever()

    def run_services():
        print("services running...")
        Services.start()

    managerd_thread = threading.Thread(target=run_managerd)
    apid_thread = threading.Thread(target=run_apid)
    systemxd_thread = threading.Thread(target=run_systemxd)
    services_thread = threading.Thread(target=run_services)

    managerd_thread.start()
    apid_thread.start()
    systemxd_thread.start()
    services_thread.start()

    try:
        managerd_thread.join()
        apid_thread.join()
        systemxd.join()
        services_thread.join()
    except KeyboardInterrupt:
        pass
    finally:
        managerd.shutdown()
        apid.shutdown()
        systemxd.shutdown()

    Services.stop()
    managerd.server_close()
    apid.server_close()
    systemxd.server_close()

    SystemXManager.uinitialize()
elif "--dashboard" in sys.argv:
    httpd = SystemXHTTPServer(("localhost", 80))

    try:
        print("httpd listen...")
        httpd.serve_forever()
    except KeyboardInterrupt:
            pass
    httpd.server_close()
else:
    print("Unknown :", sys.argv)
