const virtualization = {
  template: `<div class="container-fluid">
  <div class="row p-3">
    <div class="col-2 ml-auto">
      <button type="button" class="btn btn-success">Add</button>

      <div class="btn-group" role="group">
        <button type="button" class="btn btn-danger">Remove</button>
        <button type="button" class="btn btn-secondary">Right</button>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>Mark</td>
            <td>Otto</td>
            <td>@mdo</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Jacob</td>
            <td>Thornton</td>
            <td>@fat</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Larry</td>
            <td>the Bird</td>
            <td>@twitter</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>`,
  data() {
    return {
    }
  },
  mounted() {
    /*axios.get(server_api + "/api/node/" + this.$route.params.uid + "/livebox/leds").then(response => {
      this.leds = response.data
    })*/
  }
}

SystemX.register("virtualization", {
  application: {
    name: "Virtualization",
    component: virtualization
  }
})
