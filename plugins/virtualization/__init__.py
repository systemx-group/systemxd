from plugins.plugins import Plugins
from plugins.plugin import Plugin

class VirtualizationPlugin(Plugin):
    Version = "1.0"
    Name = "Virtualization"
    Maintainer = "stidofficial"
    Website = "stidofficial.net"

    def initialize(self):
        pass

    def uninitialize(self):
        pass

Plugins.register(VirtualizationPlugin)
