import os
import importlib

class Plugins:
    plugins = {}

    def initialize():
        for plugin in os.listdir("plugins"):
            if plugin == "__pycache__" or not os.path.isdir(os.path.join("plugins", plugin)):
                continue

            Plugins.load(plugin)

        for key, value in Plugins.plugins.items():
            print("[plugin][%s][%s] initialize" % (key, value.Maintainer))
            value.initialize()

    def load(name):
        importlib.import_module("plugins.%s" % (name, ))

    def register(plugin):
        Plugins.plugins[plugin.Name] = plugin()

    def get(plugin_name):
        return Plugins.plugins[plugin_name]

    def uninitialize():
        for key, value in Plugins.plugins.items():
            print("[plugin][%s][%s] uninitialize" % (key, value.Maintainer))
            value.uninitialize()
