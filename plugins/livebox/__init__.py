from plugins.plugins import Plugins
from plugins.plugin import Plugin

from manager.connectors import Connectors
from plugins.livebox.connector import LiveboxConnector
from plugins.livebox.api import LiveboxAPI
from manager.api import ManagerAPI
from manager.api.handler import HTTPMethod

class LiveboxPlugin(Plugin):
    Version = "1.0"
    Name = "Livebox"
    Maintainer = "stidofficial"
    Website = "stidofficial.net"

    def initialize(self):
        Connectors.register("livebox", LiveboxConnector)

        ManagerAPI.add_route(HTTPMethod.GET, r"^/api/node/(?P<uid>[a-z0-9-]+)/livebox/orange_iptv", LiveboxAPI.get_orange_iptv)
        ManagerAPI.add_route(HTTPMethod.GET, r"^/api/node/(?P<uid>[a-z0-9-]+)/livebox/leds", LiveboxAPI.get_leds)

    def uninitialize(self):
        pass

Plugins.register(LiveboxPlugin)
