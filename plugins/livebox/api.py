from manager.nodes import Nodes

class LiveboxAPI:
    def get_orange_iptv(handler):
        uid = handler.group("uid")

        node = Nodes.get(uid)
        if not node:
            handler.do_error(404, "Node not found")
            return

        handler.do_response(200, {
            "status": node.get_extradata().get("livebox_iptv_status"),
            "config": node.get_extradata().get("livebox_iptv_config")
        })

    def get_leds(handler):
        uid = handler.group("uid")

        node = Nodes.get(uid)
        if not node:
            handler.do_error(404, "Node not found")
            return

        handler.do_response(200, node.get_extradata().get("livebox_leds"))
