class NeMo:
    def __init__(self, sysbus):
        self.sysbus = sysbus

    def get_url(self, name):
        return self.sysbus.get_url("NeMo%s" % (name,))

    def post(self, name):
        return self.sysbus.post(self.get_url(name))

    def get_mibs(self, name):
        return self.sysbus.sah.post(self.get_url(".Intf.%s:getMIBs" % (name,)))
