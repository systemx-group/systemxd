class UserManagement:
    def __init__(self, sysbus):
        self.sysbus = sysbus

    def get_url(self, name):
        return self.sysbus.get_url("UserManagement%s" % (name,))

    def post(self, name):
        return self.sysbus.post(self.get_url(name))

    def get_users(self):
        return self.sysbus.sah.post(self.get_url(":getUsers"))["status"]

    def get_groups(self):
        return self.sysbus.sah.post(self.get_url(":getGroups"))["status"]
