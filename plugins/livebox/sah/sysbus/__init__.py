from plugins.livebox.sah.sysbus.nmc import NMC
from plugins.livebox.sah.sysbus.nemo import NeMo
from plugins.livebox.sah.sysbus.usermanagement import UserManagement
from plugins.livebox.sah.sysbus.voiceservice import VoiceService

class Sysbus:
    def __init__(self, sah):
        self.sah = sah
        self.NMC = NMC(self)
        self.NeMo = NeMo(self)
        self.UserManagement = UserManagement(self)
        self.VoiceService = VoiceService(self)

    def get_url(self, name):
        return self.sah.get_url("sysbus.%s" % (name, ))

    def post(self, url):
        data = self.sah.post(url)
        return self.sah.result_data(data)

    def get_devices(self):
        return self.post(self.get_url("Devices:get"))

    def device_info(self):
        return self.sah.get(self.get_url("DeviceInfo"))

    def devices(self):
        return self.sah.post(self.get_url("Devices"))["status"]

    def get_LED(self):
        return self.sah.get(self.get_url("LED"))
