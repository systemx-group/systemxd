class VoiceService:
    def __init__(self, sysbus):
        self.sysbus = sysbus

    def get_url(self, name):
        return self.sysbus.get_url("VoiceService%s" % (name,))

    def post(self, name):
        return self.sysbus.post(self.get_url(name))

    def get_handsets(self):
        return self.sysbus.sah.post(self.get_url(".VoiceApplication:listHandsets"))["status"]

    def get_list_trunks(self):
        return self.sysbus.sah.post(self.get_url(".VoiceApplication:listTrunks"))["status"]
