class NMC:
    def __init__(self, sysbus):
        self.sysbus = sysbus

    def get_url(self, name):
        return self.sysbus.get_url("NMC%s" % (name,))

    def post(self, name):
        return self.sysbus.post(self.get_url(name))

    def get_wan_status(self):
        return self.post(":getWANStatus")

    def get_lan_ip(self):
        return self.post(":getLANIP")

    def get(self):
        return self.sysbus.sah.post(self.get_url(":get"))["status"]

    def get_ipv6(self):
        return self.post(".IPv6:get")

    def get_wifi(self):
        return self.sysbus.sah.post(self.get_url(".Wifi:get"))["status"]

    def get_wifi_stats(self):
        return self.post(".Wifi:getStats")

    def get_orange_iptv_status(self):
        return self.post(".OrangeTV:getIPTVStatus")

    def get_orange_iptv_config(self):
        return self.sysbus.sah.post(self.get_url(".OrangeTV:getIPTVConfig"))["status"]

    def get_orangetv_iptv_multi_screens(self):
        return self.post(".OrangeTV:getIPTVMultiScreens")

    def get_voip_config(self):
        return self.sysbus.sah.post(self.get_url(":getVoIPConfig"))["status"]

    def get_networkconfig(self):
        return self.sysbus.sah.post(self.get_url(".NetworkConfig:get"))["status"]

    def check_for_upgrades(self):
        return self.post(":checkForUpgrades")
