import requests
import urllib.parse

from plugins.livebox.sah.sysbus import Sysbus

class SahException(Exception):
    def __init__(self, error, description, info):
        super().__init__(description)

        self.code = error
        self.description = description
        self.info = info

class Sah:
    BASE_URL = "http://%s/%s"

    def __init__(self, address):
        self.address = address
        self.session = requests.Session()
        self.contextID = None
        self.sysbus = Sysbus(self)

    def get_url(self, path, data = None):
        url = Sah.BASE_URL % (self.address, path.replace(".", "/"))
        if data is not None:
            url += "?" + urllib.parse.urlencode(data)

        return url

    def get_headers(self):
        return {
            "X-Context": self.contextID,
            "X-Prototype-Version": "1.7",
            "X-Requested-With": "XMLHttpRequest",
            "X-Sah-Request-Type": "idle"
        }

    def result_data(self, json_data):
        if json_data["status"] != False:
            return json_data["data"]

        if "errors" in json_data:
            error = json_data["errors"][0]
            raise SahException(error["error"], error["description"], error["info"])

        return None

    def post(self, url, data = None):
        response = self.session.post(url, data = data, headers = self.get_headers())
        response.raise_for_status()

        return response.json()

    def get(self, url):
        response = self.session.get(url, headers = self.get_headers())
        response.raise_for_status()

        return response.json()

    def is_logged(self):
        response = self.session.get(self.get_url(""), allow_redirects = False)
        if response.status_code == 307:
            return False

        return True

    def authenticate(self, username, password):
        post_data = {
            "username": username,
            "password": password
        }

        data = self.post(self.get_url("authenticate", post_data), data = "")
        if data["status"] == 1:
            raise Exception("Failed to login")

        self.contextID = data["data"]["contextID"]

    def get_iptv_status(self):
        return self.post(self.get_url("sysbus.NMC.rangeTV:getIPTVStatus"))
