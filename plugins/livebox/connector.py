from manager.connector import Connector
from plugins.livebox.sah import Sah
import manager
from manager.node import NodeLog
import traceback

class LiveboxConnector(Connector):
    sah = None

    def initialize(self):
        self.sah = Sah(self.node.get_remote_address())

        if not self.node.get_registry().exist("livebox_username"):
            self.node.get_registry().register("livebox_username", "admin")

        if not self.node.get_registry().exist("livebox_password"):
            self.node.get_registry().register("livebox_password")

        self.node.enable_hardwares()
        self.node.enable_networks()

        if not self.node.get_networks().interfaces.exist("lo"):
            self.node.get_networks().interfaces.add("lo")

        if not self.node.get_networks().interfaces.exist("dsl0"):
            self.node.get_networks().interfaces.add("dsl0")

        if not self.node.get_networks().interfaces.exist("eth0"):
            self.node.get_networks().interfaces.add("eth0")

        if not self.node.get_networks().interfaces.exist("wl0"):
            self.node.get_networks().interfaces.add("wl0")

        if not self.node.get_networks().interfaces.exist("wl1"):
            self.node.get_networks().interfaces.add("wl1")

    def get_name(self):
        return "livebox"

    def update(self):
        username = self.node.get_registry().get("livebox_username")
        password = self.node.get_registry().get("livebox_password")

        if not username or not password:
            self.log(NodeLog.ERROR, "No livebox_username or livebox_password set")
            return

        if not self.sah.is_logged():
            try:
                self.sah.authenticate(username, password)
                self.node.set_status(True)

                self.log(NodeLog.INFO, "Logged with %s !" % (username, ))
            except Exception as e:
                self.node.set_status(False)

                self.log(NodeLog.ERROR, "Authenticate : %s" % (str(e),))
                return

        try:
            device_info = self.sah.sysbus.device_info()
            for parameter in device_info["parameters"]:
                if parameter["name"] == "UpTime":
                    self.node.set_uptime(parameter["value"])
            #print(device_info["parameters"])

            lan_mibs = self.sah.sysbus.NeMo.get_mibs("lan")
            #print(lan_mibs)

            interfaces = list(lan_mibs["status"]["base"].keys())
            for interface in self.node.get_networks().interfaces:
                if not interface.name in interfaces:
                    self.node.get_networks().interfaces.remove(interface.name)

            for interface in interfaces:
                if not self.node.get_networks().interfaces.exist(interface):
                    self.node.get_networks().interfaces.add(interface)

            #data_mibs = self.sah.sysbus.NeMo.get_mibs("data")
            #print(data_mibs)

            wan_status = self.sah.sysbus.NMC.get_wan_status()
            #print(wan_status)

            leds = []

            led = self.sah.sysbus.get_LED()
            for led_instance in led["instances"]:
                leds.append({
                    "name": led_instance["parameters"][0]["value"],
                    "color": led_instance["parameters"][3]["value"],
                    "state": led_instance["parameters"][4]["value"]
                })

            self.node.get_extradata()["livebox_leds"] = leds

            #wifi = self.sah.sysbus.NMC.get_wifi()
            #print(wifi)

            self.node.get_extradata()["livebox_iptv_status"] = self.sah.sysbus.NMC.get_orange_iptv_status()
            self.node.get_extradata()["livebox_iptv_config"] = self.sah.sysbus.NMC.get_orange_iptv_config()

            users = self.sah.sysbus.UserManagement.get_users()
            """for user in users:
                print(user["name"])
                for group in user["groups"]:
                    print(group)"""
        except:
            traceback.print_exc()
            self.node.set_status(False)

    def uninitialize(self):
        pass
