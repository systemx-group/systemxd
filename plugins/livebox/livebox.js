const livebox_orangetv = {
  template: `<div class="tab-pane show active">
    <h4>OrangeTV</h4>

    <table class="table table-borderless">
      <tbody>
        <tr>
          <th scope="row">Status</th>
          <td>
            <span class="badge badge-success" v-if="status.IPTVStatus === 'Available'">Available</span>
            <span class="badge badge-danger" v-else>{{ status.IPTVStatus }}</span>
          </td>
        </tr>
      </tbody>
    </table>

    <table class="table">
      <thead>
        <tr>
          <th scope="col">Status</th>
          <th scope="col">Type</th>
          <th scope="col">Number</th>
          <th scope="col">Flags</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="channel in config">
          <th scope="row">
            <span class="badge badge-success" v-if="channel.ChannelStatus">Up</span>
            <span class="badge badge-danger" v-else>Down</span>
          </th>
          <td>{{ channel.ChannelType }}</td>
          <td>{{ channel.ChannelNumber }}</td>
          <td>{{ channel.ChannelFlags }}</td>
        </tr>
      </tbody>
    </table>
  </div>`,
  data() {
    return {
      status: null,
      config: null
    }
  },
  mounted() {
    axios.get(server_api + "/api/node/" + this.$route.params.uid + "/livebox/orange_iptv").then(response => {
      this.status = response.data.status
      this.config = response.data.config
    })
  }
}

const livebox_leds = {
  template: `<div class="tab-pane show active">
    <h4>Leds</h4>

    <table class="table">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">Color</th>
          <th scope="col">State</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="led in leds">
          <th scope="row">{{ led.name }}</th>
          <td>{{ led.color }}</td>
          <td>{{ led.state }}</td>
        </tr>
      </tbody>
    </table>
  </div>`,
  data() {
    return {
      leds: null
    }
  },
  mounted() {
    axios.get(server_api + "/api/node/" + this.$route.params.uid + "/livebox/leds").then(response => {
      this.leds = response.data
    })
  }
}

SystemX.register("livebox", {
  node_tabs: [
    {
      name: "Livebox - OrangeTV",
      alias: "orangetv",
      connector_only: true,
      component: livebox_orangetv
    },
    {
      name: "Livebox - Leds",
      alias: "leds",
      connector_only: true,
      component: livebox_leds
    }
  ]
})
