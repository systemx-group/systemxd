from plugins.plugins import Plugins
from plugins.plugin import Plugin

from manager.connectors import Connectors
from plugins.snmp.connector import SNMPConnector
from manager.services import Services
from plugins.snmp.service import SNMPService

class LiveboxPlugin(Plugin):
    Version = "1.0"
    Name = "SNMP"
    Maintainer = "stidofficial"
    Website = "stidofficial.net"

    def initialize(self):
        Connectors.register("snmp", SNMPConnector)
        Services.register("snmp", SNMPService)

    def uninitialize(self):
        pass

Plugins.register(LiveboxPlugin)
