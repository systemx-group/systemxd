from pysnmp.entity import engine, config
from pysnmp.carrier.asyncore.dgram import udp
from pysnmp.entity.rfc3413 import ntfrcv

class SNMPService:
    def __init__(self):
        self.__snmpEngine = engine.SnmpEngine()

        config.addTransport(
            self.__snmpEngine,
            udp.domainName + (1,),
            udp.UdpTransport().openServerMode(('127.0.0.1', 162))
        )

        config.addTransport(
            self.__snmpEngine,
            udp.domainName + (2,),
            udp.UdpTransport().openServerMode(('127.0.0.1', 2162))
        )

        config.addV1System(self.__snmpEngine, 'my-area', 'public')

        ntfrcv.NotificationReceiver(self.__snmpEngine, SNMPService.cbFun)

        self.__snmpEngine.transportDispatcher.jobStarted(1)

    def cbFun(snmpEngine, stateReference, contextEngineId, contextName,
              varBinds, cbCtx):
        print('Notification from ContextEngineId "%s", ContextName "%s"' % (contextEngineId.prettyPrint(),
                                                                            contextName.prettyPrint()))
        for name, val in varBinds:
            print('%s = %s' % (name.prettyPrint(), val.prettyPrint()))


    def start(self):
        try:
            self.__snmpEngine.transportDispatcher.runDispatcher()
        except:
            self.__snmpEngine.transportDispatcher.closeDispatcher()
            raise

    def stop(self):
        self.__snmpEngine.transportDispatcher.jobFinished(1)
        self.__snmpEngine.transportDispatcher.closeDispatcher()
