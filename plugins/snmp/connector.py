from manager.connector import Connector
import manager
from manager.node import NodeLog

from plugins.snmp.snmp import SnmpObject, SnmpException, Snmp
import math

class SNMPConnector(Connector):
    def initialize(self):
        self.__snmp = Snmp(self.node.get_remote_address(), "public", 0)

    def get_name(self):
        return "snmp"

    def update(self):
        try:
            varBinds = self.__snmp.get((
                SnmpObject("SNMPv2-MIB", "sysDescr", 0),
                SnmpObject("SNMPv2-MIB", "sysName", 0),
                SnmpObject("SNMPv2-MIB", "sysLocation", 0),
                SnmpObject("SNMPv2-MIB", "sysContact", 0),
                SnmpObject("SNMPv2-MIB", "sysUpTime", 0)
#                SnmpObject("1.3.6.1.2.1.2.2")
            ))

            for varBind in varBinds:
                name, value = varBind

                if name.prettyPrint() == "SNMPv2-MIB::sysName.0":
                    self.node.set_name(value.prettyPrint())
                elif name.prettyPrint() == "SNMPv2-MIB::sysUpTime.0":
                    self.node.set_uptime(math.floor(int(value.prettyPrint()) / 100))
                else:
                    print(varBind)

            """varBinds = self.__snmp.get("IF-MIB", "ifDescr")

            for varBind in varBinds:
                print(" = ".join([x.prettyPrint() for x in varBind]))"""

            self.node.set_status(True)
        except SnmpException as e:
            print(e)
            self.node.set_status(False)

    def uninitialize(self):
        pass
