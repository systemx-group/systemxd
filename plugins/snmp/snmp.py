from pysnmp.hlapi import *

class SnmpObject(ObjectIdentity):
    pass

class SnmpException(Exception):
    pass

class Snmp:
    def __init__(self, address, community, version):
        self.__engine = SnmpEngine()
        self.__community = CommunityData(community, mpModel=version)

        self.__udptransporttarget = UdpTransportTarget((address, 161))

    def get(self, objects):
        objectsTyped = []

        for object in objects:
            objectsTyped.append(ObjectType(object))

        g = getCmd(self.__engine,
                self.__community,
                self.__udptransporttarget,
                ContextData(),
                *objectsTyped,
                lookupNames=True, lookupValues=True)

        errorIndication, errorStatus, errorIndex, varBinds = next(g)

        if errorIndication:
            raise SnmpException(errorIndication)
        elif errorStatus:
            raise SnmpException("%s at %s" % (errorStatus.prettyPrint(),
                                errorIndex and varBinds[int(errorIndex) - 1][0] or "?"))
        else:
            return varBinds
