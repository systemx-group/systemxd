CREATE TABLE IF NOT EXISTS nodes(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  uid TEXT UNIQUE,
  remote_address TEXT NOT NULL UNIQUE,
  name TEXT NOT NULL,
  status BOOLEAN DEFAULT FALSE,
  type INTEGER NOT NULL,
  manager BOOLEAN DEFAULT FALSE,
  token TEXT DEFAULT NULL,
  connector TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS nodes_logs(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  node TEXT,
  timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,
  type INTEGER,
  class TEXT,
  text TEXT,
  FOREIGN KEY(node) REFERENCES nodes(uid)
);

CREATE TABLE IF NOT EXISTS nodes_registry(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  node TEXT,
  key TEXT,
  value TEXT,
  FOREIGN KEY(node) REFERENCES nodes(uid)
);
