class ManagerAPI:
    _routes = []

    def add_route(method, path, callback):
        ManagerAPI._routes.append({
            "method": method,
            "path": path,
            "callback": callback
        })

    def handle(handler, method):
        for route in ManagerAPI._routes:
            if handler.match(route["path"]):
                if method == HTTPMethod.OPTIONS:
                    handler.do_response(200)
                    return
                elif route["method"] == method:
                    route["callback"](handler)
                    return

        handler.do_error(404, "Not route found")

from manager.api.handler import HTTPMethod
