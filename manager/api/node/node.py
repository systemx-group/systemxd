from manager.nodes import Nodes

class APINode:
    def logs(handler):
        uid = handler.group("uid")
        offset = handler.param("offset", int, 0)
        limit = handler.param("limit", int, 10)

        node = Nodes.get(uid)
        if not node:
            print("No node found with", uid)
            return

        handler.do_response(200, node.get_logs().find(offset, limit))
