from manager.nodes import Nodes

class APINodeNetworksInterfaces:
    def get(handler):
        uid = handler.group("uid")

        node = Nodes.get(uid)
        if not node:
            print("No node found with", uid)
            return

        interfaces = []
        for interface in node.get_networks().interfaces:
            interfaces.append({
                "name": interface.name
            })

        handler.do_response(200, interfaces)
