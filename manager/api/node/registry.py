from manager.nodes import Nodes

class APINodeRegistry:
    def get(handler):
        uid = handler.group("uid")

        node = Nodes.get(uid)
        if not node:
            print("No node found with", uid)
            return

        handler.do_response(200, dict(node.get_registry()))

    def set(handler):
        uid = handler.group("uid")
        name = handler.param("name", str)
        value = handler.param("value", str)

        node = Nodes.get(uid)
        if not node:
            print("No node found with", uid)
            return

        node.registry.set(name, value)

        handler.do_response(200)
