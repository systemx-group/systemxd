from plugins.plugins import Plugins

class APIPlugins:
    def list(handler):
        plugins = []
        for key, value in Plugins.plugins.items():
            plugins.append({
                "name": value.Name,
                "version": value.Version,
                "maintainer": value.Maintainer,
                "website": value.Website
            })

        handler.do_response(200, plugins)
