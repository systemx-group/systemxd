from manager.connectors import Connectors

class APIConnectors:
    def list(handler):
        handler.do_response(200, list(Connectors.connectors.keys()))
