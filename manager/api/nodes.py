from manager.nodes import Nodes

class APINodes:
    def add(handler):
        address = handler.param("address", str)
        type = handler.param("type", int)
        connector = handler.param("connector", str)

        node = Nodes.add(address, type, connector)

        handler.do_response(201, {
            "uid": node.get_uid()
        })

    def remove(handler):
        uid = handler.group("uid")

        Nodes.remove(uid)

        handler.do_response(200)

    def get(handler):
        uid = handler.group("uid")

        node = Nodes.get(uid)

        if node == None:
            handler.do_response(404)
            return

        handler.do_response(200, {
            "uid": node.get_uid(),
            "name": node.get_name(),
            "remote_address": node.get_remote_address(),
            "token": node.get_token(),
            "status": node.get_status(),
            "uptime": node.get_uptime(),
            "connector": node.get_connector().get_name() if node.get_connector() != None else None,
            "hardwares": node.get_hardwares() != None,
            "softwares": node.get_softwares() != None,
            "networks": node.get_networks() != None,
            "smbios": node.get_smbios() != None,
            "ipmi": node.get_ipmi() != None,
            "logs": node.get_logs().count()
        })

    def find(handler):
        text = handler.param("text", str, "")
        type = handler.param("type", int)
        offset = handler.param("offset", int)
        limit = handler.param("limit", int)

        results = []
        for node_row in Nodes.find(text, type, offset, limit):
            results.append({
                "uid": node_row["uid"],
                "status": node_row["status"],
                "name": node_row["name"],
                "status": node_row["status"],
                "type": node_row["type"],
                "manager": node_row["manager"],
            })

        handler.do_response(200, {
            "results": results,
            "offset": offset,
            "limit": limit,
            "count": Nodes.count(text, type)
        })
