from http.server import BaseHTTPRequestHandler
import json
from urllib.parse import urlparse, parse_qs
import re
import traceback

from manager.api.node.node import APINode
from manager.api.node.networks.interfaces import APINodeNetworksInterfaces
from manager.api.node.registry import APINodeRegistry
from manager.api.nodes import APINodes
from manager.api.connectors import APIConnectors
from manager.api.plugins import APIPlugins
from manager.api import ManagerAPI

class HTTPMethod:
    OPTIONS = 0,
    GET = 1,
    HEAD = 2,
    POST = 3,
    PUT = 4,
    DELETE = 5,
    TRACE = 6,
    CONNECT = 7

class SystemXAPIRequestHandler(BaseHTTPRequestHandler):
    def parse(self):
        self.path_parsed = urlparse(self.path)
        self.params = parse_qs(self.path_parsed.query)

        if "Content-Length" in self.headers:
            content_length = int(self.headers["Content-Length"])

            self.params = json.loads(self.rfile.read(content_length))

    def parsed_path(self):
        return self.path_parsed.path

    def param(self, name, type, default = None):
        if name in self.params:
            if isinstance(self.params[name], list) and self.params[name][0]:
                return type(self.params[name][0]) if type(self.params[name][0]) is not None else default
            else:
                return self.params[name] if self.params[name] is not None  else default

        return default

    def do_response(self, status, data = {}):
        json_data = json.dumps(data)

        self.send_response(status)
        self.send_header("Content-Type", "application/json")
        self.send_header("Content-Length", str(len(json_data)))
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header("Access-Control-Allow-Headers", "*")
        self.send_header("Access-Control-Allow-Methods", "*")
        self.end_headers()

        self.wfile.write(json_data.encode())

    def do_error(self, status, message = None):
        self.do_response(status, {
            "message": message
        })

    def match(self, regex):
        regex = re.compile(regex)
        self.regex_match = regex.match(self.parsed_path())

        return self.regex_match

    def group(self, key):
        return self.regex_match.group(key)

    def do_request(self, method):
        self.parse()

        ManagerAPI.add_route(HTTPMethod.GET, "/api/nodes", APINodes.find)
        ManagerAPI.add_route(HTTPMethod.PUT, "/api/nodes/add", APINodes.add)

        ManagerAPI.add_route(HTTPMethod.GET, r"^/api/node/(?P<uid>[a-z0-9-]+)$", APINodes.get)
        ManagerAPI.add_route(HTTPMethod.DELETE, r"^/api/node/(?P<uid>[a-z0-9-]+)$", APINodes.remove)
        ManagerAPI.add_route(HTTPMethod.GET, r"^/api/node/(?P<uid>[a-z0-9-]+)/logs$", APINode.logs)
        ManagerAPI.add_route(HTTPMethod.GET, r"^/api/node/(?P<uid>[a-z0-9-]+)/networks/interfaces$", APINodeNetworksInterfaces.get)

        ManagerAPI.add_route(HTTPMethod.GET, r"^/api/node/(?P<uid>[a-z0-9-]+)/registry", APINodeRegistry.get)
        ManagerAPI.add_route(HTTPMethod.POST, r"^/api/node/(?P<uid>[a-z0-9-]+)/registry", APINodeRegistry.set)

        ManagerAPI.add_route(HTTPMethod.GET, "/api/connectors", APIConnectors.list)

        ManagerAPI.add_route(HTTPMethod.GET, "/api/plugins", APIPlugins.list)

        try:
            ManagerAPI.handle(self, method)
        except Exception as e:
            self.send_error(503)
            traceback.print_exc()

    def do_GET(self):
        self.do_request(HTTPMethod.GET)

    def do_POST(self):
        self.do_request(HTTPMethod.POST)

    def do_PUT(self):
        self.do_request(HTTPMethod.PUT)

    def do_DELETE(self):
        self.do_request(HTTPMethod.DELETE)

    def do_OPTIONS(self):
        self.do_request(HTTPMethod.OPTIONS)
