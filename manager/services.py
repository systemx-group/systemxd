import threading

class Services:
    services = {}
    serviceThreads = []

    def register(name, service_class):
        Services.services[name] = service_class()

        serviceThread = threading.Thread(target=Services.services[name].start)
        Services.serviceThreads.append(serviceThread)

    def start():
        for serviceThread in Services.serviceThreads:
            serviceThread.start()

    def join():
        for serviceThread in Services.serviceThreads:
            serviceThread.join()

    def stop():
        for name, service in Services.services.items():
            service.stop()

        Services.join()
