import sqlite3

class Database:
    def __init__(self):
        self.conn = sqlite3.connect("systemxd.db", check_same_thread=False)

        def dict_factory(cursor, row):
            d = {}
            for idx, col in enumerate(cursor.description):
                d[col[0]] = row[idx]
            return d

        self.conn.row_factory = dict_factory

    def execute(self, sql, *args):
        cursor = self.conn.cursor()
        return cursor.execute(sql, args)

    def commit(self):
        return self.conn.commit()

    def load(self, path):
        sql_file = open(path, "r")
        self.conn.executescript(sql_file.read())
        sql_file.close()

    def __del__(self):
        self.conn.close()
