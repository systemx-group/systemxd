import manager
import uuid
from manager.node import NodeType, Node

class Nodes:
    nodes = []
    nodes_thread = None

    def initialize():
        nodes_cursor = manager.SystemXManager.db.execute("SELECT * FROM nodes")
        for node_row in nodes_cursor:
            Nodes.nodes.append(Node(node_row["uid"], node_row["name"], node_row["remote_address"],
                                    node_row["token"], node_row["type"], node_row["connector"], True))

    def add(remote_address, type, connector):
        uid = uuid.uuid4()
        name = "%s-%s" % (NodeType.get_by_id(type), str(uid))

        node_cursor = manager.SystemXManager.db.execute("INSERT INTO nodes(uid, remote_address, name, type, connector) VALUES(?, ?, ?, ?, ?)",
                                                        str(uid), remote_address, name, type, connector)
        manager.SystemXManager.db.commit()

        node_id = node_cursor.lastrowid
        node_cursor = manager.SystemXManager.db.execute("SELECT uid FROM nodes WHERE id = ?", node_id)
        node = Node(node_cursor.fetchone()["uid"], name, remote_address, None, type, connector, True)

        Nodes.nodes.append(node)

        return node

    def get(uid):
        for node in Nodes.nodes:
            if uid == node.get_uid():
                return node

        return None

    def remove(uid):
        manager.SystemXManager.db.execute("DELETE FROM nodes WHERE uid = ?", uid)
        manager.SystemXManager.db.commit()

        for node in Nodes.nodes:
            if uid == node.get_uid():
                Nodes.remove(node)
                break

    def find(text, type, offset, limit):
        return manager.SystemXManager.db.execute("SELECT uid, name, status, type, manager FROM nodes WHERE name LIKE ? AND (type = ? OR ? = 0) LIMIT ?,?", ("%" + text + "%"), type, type, offset, limit)

    def count(text, type):
        count_cursor = manager.SystemXManager.db.execute("SELECT COUNT(*) AS count FROM nodes WHERE name LIKE ? AND (type = ? OR ? = 0)", ("%" + text + "%"), type, type)

        return count_cursor.fetchone()["count"]

    def uninitialize():
        for node in Nodes.nodes:
            node.stop()
            del node
