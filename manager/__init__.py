from socketserver import TCPServer, UDPServer
from manager.handler import SystemXManagerRequestHandler

from http.server import HTTPServer
from manager.api.handler import SystemXAPIRequestHandler

from manager.database import Database
from manager.nodes import Nodes

from plugins.plugins import Plugins

from manager.server.handler import SystemXRequestHandler

class SystemXManager:
    def initialize():
        SystemXManager.db = Database()
        SystemXManager.db.load("systemxd.sql")

        Plugins.initialize()
        Nodes.initialize()

    def uinitialize():
        Nodes.uninitialize()
        Plugins.uninitialize()

class SystemXManagerServer(TCPServer):
    def __init__(self, address):
        super().__init__(address, SystemXManagerRequestHandler)

class SystemXAPIServer(HTTPServer):
    def __init__(self, address):
        super().__init__(address, SystemXAPIRequestHandler)

class SystemXServer(UDPServer):
    def __init__(self, address):
        super().__init__(address, SystemXRequestHandler)
