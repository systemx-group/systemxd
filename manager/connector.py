import os
import datetime

class Connector:
    def __init__(self, node):
        self.node = node

    def initialize(self):
        pass

    def get_name(self):
        return type(self).__name__

    def log(self, log_type, *args):
        self.node.get_logs().log(log_type, type(self).__name__, *args)

    def update(self):
        pass

    def hardwares(self):
        raise None

    def networks(self):
        raise None

    def SMBios(self):
        raise None

    def uninitialize(self):
        pass
