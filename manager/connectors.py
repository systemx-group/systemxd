class Connectors:
    connectors = {}

    def register(name, connector_class):
        Connectors.connectors[name] = connector_class

    def get(name):
        return Connectors.connectors[name]
