from manager.node.networks.interface import NodeNetworksInterface

class NodeNetworksInterfaces(list):
    def __init__(self, networks):
        self.networks = networks

    def exist(self, name):
        for interface in self:
            if interface.name == name:
                return True

        return False

    def add(self, name):
        interface = NodeNetworksInterface(name)
        self.append(interface)

        return interface

    def remove(self, name):
        interface = self.get(name)
        super().remove(interface)

    def get(self, name):
        for interface in self:
            if interface.name == name:
                return interface

        return None
