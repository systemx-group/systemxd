from manager.node.networks.interfaces import NodeNetworksInterfaces

class NodeNetworks:
    def __init__(self, node):
        self.interfaces = NodeNetworksInterfaces(self)
