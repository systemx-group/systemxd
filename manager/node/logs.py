import manager

class NodeLogs:
    def __init__(self, node):
        self.node = node

    def log(self, log_type, name, *args):
        manager.SystemXManager.db.execute("""
            INSERT INTO nodes_logs(node, type, class, text)
            VALUES (?, ?, ?, ?)""",
            self.node.get_uid(), log_type, name, " ".join(args))
        manager.SystemXManager.db.commit()

    def count(self):
        logs_count_cursor = manager.SystemXManager.db.execute("""
            SELECT COUNT(*) as count
            FROM nodes_logs
            WHERE node = ?""",
            self.node.get_uid())

        return logs_count_cursor.fetchone()["count"]

    def find(self, offset, limit):
        logs_cursor = manager.SystemXManager.db.execute("""
            SELECT timestamp, type, class, text
            FROM nodes_logs
            WHERE node = ?
            ORDER BY timestamp DESC
            LIMIT ?, ?""",
            self.node.get_uid(), offset, limit)

        return {
            "results": logs_cursor.fetchall(),
            "offset": offset,
            "limit": limit,
            "count": self.count()
        }
