import manager

class NodeRegistry(dict):
    def __init__(self, node):
        self.node = node

        node_registry_cursor = manager.SystemXManager.db.execute("SELECT key, value FROM nodes_registry WHERE node = ?", self.node.get_uid())
        for register in node_registry_cursor.fetchall():
            self[register["key"]] = register["value"]

    def register(self, key, value = None):
        self[key] = value

        manager.SystemXManager.db.execute("INSERT INTO nodes_registry(node, key, value) VALUES(?, ?, ?)", self.node.get_uid(), key, value)
        manager.SystemXManager.db.commit()

    def unregister(self, key):
        del self[key]

    def exist(self, key):
        return key in self

    def set(self, key, value):
        self[key] = value

        manager.SystemXManager.db.execute("UPDATE nodes_registry SET value = ? WHERE node = ? AND key = ?", value, self.node.get_uid(), key)
        manager.SystemXManager.db.commit()

    def get(self, key):
        return self[key]
