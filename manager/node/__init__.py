from manager.connectors import Connectors
import manager
from manager.node.hardwares import NodeHardwares
from manager.node.networks import NodeNetworks
from manager.node.logs import NodeLogs
from manager.node.registry import NodeRegistry
from manager.node.extradata import NodeExtradata
import traceback
import secrets
import threading
from datetime import timedelta

class NodeType:
    ROUTER = (1, "router")
    SWITCH = (2, "switch")
    SERVER = (3, "server")
    COMPUTER = (4, "computer")

    def get_by_id(id):
        if NodeType.ROUTER[0] == id:
            return NodeType.ROUTER[1]
        elif NodeType.SWITCH[0] == id:
            return NodeType.SWITCH[1]
        elif NodeType.SERVER[0] == id:
            return NodeType.SERVER[1]
        elif NodeType.COMPUTER[0] == id:
            return NodeType.COMPUTER[1]

class NodeLog:
    INFO = 0
    WARNING = 1
    ERROR = 2

class Node:
    INTERVAL = 160

    def __init__(self, uid, name, remote_address, token, type, connector, managed):
        self.__uid = uid
        self.__name = name
        self.__remote_address = remote_address
        self.__token = token
        self.__type = type
        self.__managed = managed

        self.__status = False
        self.__uptime = 0

        self.__hardwares = None
        self.__softwares = None
        self.__networks = None
        self.__smbios = None
        self.__ipmi = None
        self.__logs = NodeLogs(self)
        self.__registry = NodeRegistry(self)
        self.__extradata = NodeExtradata()

        self.__connector = None

        if not self.__managed:
            return

        self.__event = threading.Event()
        self.__interval = timedelta(seconds=Node.INTERVAL)
        self.__thread = threading.Thread(target=self.update)

        try:
            self.__connector = Connectors.get(connector)(self)
        except:
            print("Failed to find %s connector" % (self.connector, ))

        if self.__connector:
            try:
                self.__connector.initialize()
            except Exception as e:
                self.get_logs().log(NodeLog.ERROR, type(self.__connector).__name__, "Failed to initialize connector :", str(e))
                traceback.print_exc()

        self.__thread.start()

    def enable_hardwares(self):
        self.__hardwares = NodeHardwares(self)

    def enable_networks(self):
        self.__networks = NodeNetworks(self)

    def update(self):
        while True:
            if self.__managed and self.__connector:
                try:
                    self.__connector.update()
                except Exception as e:
                    self.get_logs().log(NodeLog.ERROR, type(self.__connector).__name__, "Failed to update connector :", str(e))
                    traceback.print_exc()

            if self.__event.wait(self.__interval.total_seconds()):
                break

    def get_uid(self):
        return self.__uid

    def get_name(self):
        return self.__name

    def set_name(self, name):
        if self.__name != name:
            manager.SystemXManager.db.execute("UPDATE nodes SET name = ? WHERE uid = ?", name, self.get_uid())
            manager.SystemXManager.db.commit()

            self.get_logs().log(NodeLog.INFO, type(self).__name__, "Change name %s to %s" % (self.__name, name))

            self.__name = name

    def get_remote_address(self):
        return self.__remote_address

    def generate_token(self):
        token = secrets.token_hex(16)

        manager.SystemXManager.db.execute("UPDATE nodes SET token = ? WHERE uid = ?", self.token, self.get_uid())
        manager.SystemXManager.db.commit()

        self.__token = token

    def get_token(self):
        return self.__token

    def get_status(self):
        return self.__status

    def set_status(self, status):
        if self.__status != status:

            manager.SystemXManager.db.execute("UPDATE nodes SET status = ? WHERE uid = ?", status, self.get_uid())
            manager.SystemXManager.db.commit()

            if status:
                self.get_logs().log(NodeLog.INFO, type(self).__name__, "Change status to up")
            else:
                self.get_logs().log(NodeLog.INFO, type(self).__name__, "Change status to down")

            self.__status = status

    def set_uptime(self, uptime):
        self.__uptime = uptime

    def get_uptime(self):
        return self.__uptime

    def get_hardwares(self):
        return self.__hardwares

    def get_softwares(self):
        return self.__softwares

    def get_networks(self):
        return self.__networks

    def get_smbios(self):
        return self.__smbios

    def get_ipmi(self):
        return self.__ipmi

    def get_logs(self):
        return self.__logs

    def get_registry(self):
        return self.__registry

    def get_extradata(self):
        return self.__extradata

    def get_connector(self):
        return self.__connector

    def stop(self):
        if self.__managed:
            self.__event.set()
            self.__thread.join()

    def __del__(self):
        if not self.__connector:
            return

        try:
            self.__connector.uninitialize()
        except Exception as e:
            self.get_logs().log(NodeLog.ERROR, type(self.__connector).__name__, "Failed to uninitialize connector :", str(e))
            traceback.print_exc()
