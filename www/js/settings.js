const settings = {
  template: `<div class="container-fluid">
  <div class="row p-2">
    <div class="col-12 d-flex justify-content-between p-2">
      <h3>Manager cluster</h3>

      <div>
        <button type="button" class="btn btn-success">Add</button>
      </div>
    </div>
    <div class="col-12">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Address</th>
            <th scope="col">Status</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>
              <span class="badge badge-success">Online</span>
            </td>
            <td>
              <button type="button" class="btn btn-danger">Remove</button>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>`
}
