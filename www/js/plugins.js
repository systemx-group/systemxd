const plugins = {
  template: `<div class="container-fluid">
  <div class="row p-2">
    <div class="col-12">
      <h4>Plugins</h4>

      <table class="table">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Version</th>
            <th scope="col">Maintainer</th>
            <th scope="col">Website</th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="plugin in plugins">
            <th scope="row">{{ plugin.name }}</th>
            <td>{{ plugin.version }}</td>
            <td>{{ plugin.maintainer }}</td>
            <td>
              <a :href="plugin.website">{{ plugin.website }}</a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>`,
  data() {
    return {
      plugins: null
    }
  },
  mounted() {
    axios.get(server_api + "/api/plugins").then(response => {
      this.plugins = response.data
    })
  }
}
