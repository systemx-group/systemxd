const node_softwares = {
  template: `<div class="tab-pane show active">
    <h4>Softwares</h4>

    <p class="lead" v-if="softwares">Package manager : Windows / APT / DNF/YUM</p>

    <table class="table" v-if="softwares">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">First</th>
          <th scope="col">Last</th>
          <th scope="col">Handle</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>@fat</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>Larry</td>
          <td>the Bird</td>
          <td>@twitter</td>
        </tr>
      </tbody>
    </table>

    <div class="alert alert-danger" role="alert" v-if="!softwares">
      Not supported !
    </div>
  </div>`,
  data() {
    return {
      softwares: null
    }
  }
}
