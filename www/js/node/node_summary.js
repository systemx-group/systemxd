const node_summary = {
  template: `<div class="tab-pane show active" role="tabpanel">
    <h4>Summary</h4>

    <table class="table table-borderless">
      <tbody>
        <tr>
          <th scope="row">ID</th>
          <td>{{ $route.params.uid }}</td>
        </tr>
        <tr>
          <th scope="row">Name</th>
          <td>{{ this.$parent.name }}</td>
        </tr>
        <tr>
          <th scope="row">Remote address</th>
          <td>{{ this.$parent.remote_address }}</td>
        </tr>
        <tr>
          <th scope="row">Token</th>
          <td if="!this.$parent.token">
            <span class="badge badge-pill badge-warning">No token generated</span>
          </td>
          <td else>{{ this.$parent.token }}</td>
        </tr>
        <tr>
          <th scope="row">Uptime</th>
          <td>{{ this.$parent.uptime | uptime }}</td>
        </tr>
        <tr>
          <th scope="row">Connector</th>
          <td>{{ this.$parent.connector }}</td>
        </tr>
      </tbody>
    </table>
  </div>`,
  filters: {
    uptime: function(value) {
      var days = Math.floor(value / (60 * 60 * 24))

      var divisor_for_hours = value % (60 * 60 * 24)
      var hours = Math.floor(divisor_for_hours / (60 * 60))

      var divisor_for_minutes = value % (60 * 60)
      var minutes = Math.floor(divisor_for_minutes / 60)

      var divisor_for_seconds = value % 60
      var seconds = Math.ceil(divisor_for_seconds)

      return days + " d " + hours + " h " +minutes + " m " + seconds + " s"
    }
  }
}
