const node_networks = {
  template: `<div class="tab-pane show active">
    <h4>Networks</h4>

    <h5>Interfaces</h5>

    <table class="table" v-if="interfaces">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">MAC Address</th>
          <th scope="col"></th>
          <th scope="col">Handle</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="interface in interfaces">
          <th scope="row">{{ interface.name }}</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
      </tbody>
    </table>

    <div class="alert alert-danger" role="alert" v-if="!this.$parent.networks">
      Not supported !
    </div>
  </div>`,
  data() {
    return {
      interfaces: null
    }
  },
  mounted() {
    axios.get(server_api + "/api/node/" + this.$route.params.uid + "/networks/interfaces").then(response => {
      this.interfaces = response.data
    })
  }
}
