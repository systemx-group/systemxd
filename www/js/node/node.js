const node = {
  template: `<div class="container-fluid">
  <div class="row py-3">
    <div class="col-lg-2 col-sm-4">
      <div class="nav flex-column nav-pills" role="tablist">
        <router-link class="nav-link" exact-active-class="active" :to="{ name: 'node_summary', params: {uid: $route.params.uid} }">
          Summary <span class="badge badge-pill badge-success float-right" v-if="status">Up</span> <span class="badge badge-pill badge-danger float-right" v-if="!status">Down</span>
        </router-link>
        <router-link class="nav-link" exact-active-class="active" :to="{ name: 'node_hardwares', params: {uid: $route.params.uid} }">
          Hardwares <span class="badge badge-pill badge-danger float-right" v-if="!hardwares">Not supported</span>
        </router-link>
        <router-link class="nav-link" exact-active-class="active" :to="{ name: 'node_softwares', params: {uid: $route.params.uid} }">
          Softwares <span class="badge badge-pill badge-danger float-right" v-if="!softwares">Not supported</span>
        </router-link>
        <router-link class="nav-link" exact-active-class="active" :to="{ name: 'node_networks', params: {uid: $route.params.uid} }">
          Networks <span class="badge badge-pill badge-danger float-right" v-if="!networks">Not supported</span>
        </router-link>
        <router-link class="nav-link" exact-active-class="active" :to="{ name: 'node_smbios', params: {uid: $route.params.uid} }">
          SMBios <span class="badge badge-pill badge-danger float-right" v-if="!smbios">Not supported</span>
        </router-link>
        <router-link class="nav-link" exact-active-class="active" :to="{ name: 'node_ipmi', params: {uid: $route.params.uid} }">
          IPMI <span class="badge badge-pill badge-danger float-right" v-if="!ipmi">Not supported</span>
        </router-link>
        <router-link class="nav-link" exact-active-class="active" :to="{ name: 'node_settings', params: {uid: $route.params.uid} }">
          Settings
        </router-link>
        <router-link class="nav-link" exact-active-class="active" :to="{ name: 'node_logs', params: {uid: $route.params.uid} }">
          Logs <span class="badge badge-pill badge-info float-right">{{ logs }}</span>
        </router-link>
        <router-link class="nav-link" exact-active-class="active" v-for="(plugin_tab, index) in this.$parent.$data.node_plugin_tabs" :key="index" v-if="plugin_tab.connector === connector" :to="{ name: plugin_tab.alias, params: {uid: $route.params.uid} }">
          {{ plugin_tab.name }}
        </router-link>
      </div>
    </div>
    <div class="col-lg-10 col-sm-8">
      <div class="tab-content">
        <router-view></router-view>
      </div>
    </div>
  </div>
</div>`,
  data() {
    return {
      name: null,
      remote_address: null,
      token: null,
      status: false,
      uptime: 0,
      connector: null,
      hardwares: false,
      softwares: false,
      networks: false,
      smbios: false,
      ipmi: false,
      logs: 0,
      search_limit: 10
    }
  },
  mounted() {
    axios.get(server_api + "/api/node/" + this.$route.params.uid).then(response => {
      this.name = response.data.name
      this.hardwares = response.data.hardwares
      this.softwares = response.data.softwares
      this.networks = response.data.networks
      this.smbios = response.data.smbios
      this.remote_address = response.data.remote_address
      this.token = response.data.token
      this.status = response.data.status
      this.uptime = response.data.uptime
      this.connector = response.data.connector
      this.logs = response.data.logs
    })
  }
}
