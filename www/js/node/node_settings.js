const node_settings = {
  template: `<div class="tab-pane show active">
    <h4>Settings</h4>

    <div class="form-group row">
      <label for="name" class="col-sm-2 col-form-label">Name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="name" v-model="this.$parent.name">
      </div>
    </div>

    <div class="form-group row">
      <label for="remote_address" class="col-sm-2 col-form-label">Remote address</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="remote_address" v-model="this.$parent.remote_address">
      </div>
    </div>

    <button type="button" class="btn btn-success">Generate token</button>

    <h5 class="pt-2">Registry</h5>

    <table class="table">
      <thead>
        <tr>
          <th scope="col">Key</th>
          <th scope="col">Value</th>
        </tr>
      </thead>
      <tbody>
        <tr v-for="(value, key) in registry">
          <th scope="row">{{ key }}</th>
          <td>
            <input type="text" class="form-control" @change="updateRegister" :name="key" :value="value">
          </td>
        </tr>
      </tbody>
    </table>
  </div>`,
  data() {
    return {
      registry: {}
    }
  },
  methods: {
    updateRegister(event) {
      axios.post(server_api + "/api/node/" + this.$route.params.uid + "/registry", {name: event.target.name, value: event.target.value})
    }
  },
  mounted() {
    axios.get(server_api + "/api/node/" + this.$route.params.uid + "/registry").then(response => {
      this.registry = response.data
    })
  }
}
