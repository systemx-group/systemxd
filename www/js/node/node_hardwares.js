const node_hardwares = {
  template: `<div class="tab-pane show active">
    <h4>Hardwares</h4>

    <table class="table" v-if="this.$parent.hardwares">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">MAC Address</th>
          <th scope="col"></th>
          <th scope="col">Handle</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Mark</td>
          <td>Otto</td>
          <td>@mdo</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>Jacob</td>
          <td>Thornton</td>
          <td>@fat</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>Larry</td>
          <td>the Bird</td>
          <td>@twitter</td>
        </tr>
      </tbody>
    </table>

    <div class="alert alert-danger" role="alert" v-if="!this.$parent.hardwares">
      Not supported !
    </div>
  </div>`,
  data() {
    return {
      hardwares: null
    }
  }
}
