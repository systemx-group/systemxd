const node_logs = {
  template: `<div class="tab-pane show active" role="tabpanel">
    <h4>Logs</h4>

    <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <form v-on:submit.prevent="search">
              <div class="form-row">
                <div class="col-11">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Log" v-model="search_text">
                    <select class="form-control" v-model="search_type">
                      <option value="0" :selected="search_type === 0">All</option>
                      <option v-for="log_type in logs_type" v-bind:value="log_type.id" :selected="search_type === log_type.id">{{ log_type.text }}</option>
                    </select>
                  </div>
                </div>
                <div class="col-1">
                  <select class="form-control" v-model="search_limit">
                    <option value="10" :selected="search_limit === 10">10</option>
                    <option value="20" :selected="search_limit === 20">20</option>
                    <option value="50" :selected="search_limit === 50">50</option>
                    <option value="100" :selected="search_limit === 100">100</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div class="col-12 pt-4">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">Timestamp</th>
                  <th scope="col">Type</th>
                  <th scope="col">Class</th>
                  <th scope="col">Text</th>
                </tr>
              </thead>
              <tbody>
                <tr v-for="log in logs">
                  <th scope="row">{{ log.timestamp }}</th>
                  <td>
                    <span class="badge badge-info">{{ log.type | type_to_text(logs_type) }}</span>
                  </td>
                  <td>
                    <span class="badge badge-pill badge-secondary">{{ log.class }}</span>
                  </td>
                  <td>{{ log.text }}</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <nav aria-label="Page navigation example">
            <ul class="pagination justify-content-center">
              <li class="page-item" v-for="p_page in pages" :class="{ active: page === p_page }">
                <a class="page-link" href="#" @click="page = p_page">{{ p_page }}</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>`,
  data() {
    return {
      logs_type: {
        0: {
          type: "primary",
          text: "Info"
        },
        1: {
          type: "warning",
          text: "Warning"
        },
        2: {
          type: "danger",
          text: "Error"
        }
      },
      logs: [],
      logs_interval_id: null,
      page: 1,
      pages: 1,
      search_text: null,
      search_type: 0,
      search_limit: 10
    }
  },
  methods: {
    get_logs() {
      axios.get(server_api + "/api/node/" + this.$route.params.uid + "/logs", {params: {text: this.search_text, type: this.search_type, offset: (this.page - 1) * this.search_limit, limit: this.search_limit}}).then(response => {
        this.logs = response.data.results
        this.page = (response.data.offset / response.data.limit) + 1
        this.pages = Math.ceil(response.data.count / response.data.limit)
      })
    }
  },
  filters: {
    type_to_class: function(value, logs_type) {
      return logs_type[value].type
    },
    type_to_text: function(value, logs_type) {
      return logs_type[value].text
    }
  },
  watch: {
    search_type: function(val) {
      this.get_logs()
    },
    search_type: function(val) {
      this.get_logs()
    },
    search_limit: function(val) {
      this.get_logs()
    },
    page: function(val) {
      this.get_logs()
    }
  },
  mounted() {
    this.get_logs()
    this.logs_interval_id = setInterval(this.get_logs, 10000)
  },
  destroyed() {
    clearInterval(this.logs_interval_id)
  }
}
