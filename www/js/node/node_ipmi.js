const node_ipmi = {
  template: `<div class="tab-pane show active">
    <h4>IPMI</h4>

    <div class="alert alert-danger" role="alert" v-if="!ipmi">
      Not supported !
    </div>
  </div>`,
  data() {
    return {
      ipmi: null
    }
  }
}
