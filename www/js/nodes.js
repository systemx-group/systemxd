const nodes = {
  template: `<div class="container-fluid">
  <div class="modal fade" id="add_node" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add node</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form>
            <div class="form-group row">
              <label for="mac_address" class="col-sm-4 col-form-label">MAC Address</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="mac_address" placeholder="01:23:45:67:89:AB">
              </div>
            </div>
            <div class="form-group row">
              <label for="address" class="col-sm-4 col-form-label">Address</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" placeholder="node.systemx.local" v-model="address">
              </div>
            </div>
            <div class="form-group row">
              <label for="type" class="col-sm-4 col-form-label">Type</label>
              <div class="col-sm-8">
                <select class="form-control" v-model="type">
                  <option v-for="node_type in node_types" v-bind:value="node_type.id">{{ node_type.name }}</option>
                </select>
              </div>
            </div>
            <div class="form-group row">
              <label for="type" class="col-sm-4 col-form-label">Connector</label>
              <div class="col-sm-8">
                <select class="form-control" v-model="connector">
                  <option v-for="connector in connectors" v-bind:value="connector">{{ connector }}</option>
                </select>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" @click="add">Add</button>
        </div>
      </div>
    </div>
  </div>
  <div class="row py-3">
    <div class="col">
      <form v-on:submit.prevent="search">
        <div class="form-row">
          <div class="col-8">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Node" @change="search($event)" v-model="search_text">
              <select class="form-control" @change="search($event)" v-model="search_type">
                <option value="0" :selected="search_type === 0">All</option>
                <option v-for="node_type in node_types" v-bind:value="node_type.id" :selected="search_type === node_type.id">{{ node_type.name }}</option>
              </select>
            </div>
          </div>
          <div class="col-1">
            <select class="form-control" @change="search($event)" v-model="search_limit">
              <option value="10" :selected="search_limit === 10">10</option>
              <option value="20" :selected="search_limit === 20">20</option>
              <option value="50" :selected="search_limit === 50">50</option>
              <option value="100" :selected="search_limit === 100">100</option>
            </select>
          </div>
          <div class="col-2">
            <div class="btn-group" role="group" aria-label="Basic example">
              <button type="button" class="btn btn-success" data-toggle="modal" data-target="#add_node">Add</button>
              <button type="button" class="btn btn-danger" @click="remove">Remove</button>
            </div>
          </div>
          <div class="col-1">
            <button type="button" class="btn btn-primary float-right" @click="search">Refresh</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="row p-2">
    <div class="col">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Status</th>
            <th scope="col">Type</th>
            <th scope="col">Manager</th>
            <th scope="col">
              <input type="checkbox" @click="check_all" v-model="checkbox_all">
            </th>
          </tr>
        </thead>
        <tbody>
          <tr v-for="node in nodes">
            <th>
              <router-link :to="{name: 'node', params: {uid: node.uid}}">{{ node.name }}</router-link>
            </th>
            <td>
              <span class="badge badge-success" v-if="node.status">Up</span>
              <span class="badge badge-danger" v-else>Down</span>
            </td>
            <td>
              <span class="badge badge-dark">{{ node.type | type_to_str(node_types) }}</span>
            </td>
            <td>
              <span class="badge badge-warning" v-if="node.manager">Manager</span>
            </td>
            <td>
              <input type="checkbox" v-model="selected_nodes" v-bind:value="node.uid">
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <div class="row">
    <div class="col">
      <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          <li class="page-item" v-for="p_page in pages" :class="{ active: page === p_page }">
            <a class="page-link" href="#" @click="page = p_page">{{ p_page }}</a>
          </li>
        </ul>
      </nav>
    </div>
  </div>
</div>`,
  data() {
    return {
      nodes: null,
      node_types: [
        {
          id: 1,
          key: "router",
          name: "Router"
        }, {
          id: 2,
          key: "switch",
          name: "Switch"
        }, {
          id: 3,
          key: "server",
          name: "Server"
        }, {
          id: 4,
          key: "computer",
          name: "Computer"
        }
      ],
      search_text: null,
      search_type: 0,
      search_limit: 10,
      checkbox_all: null,
      selected_nodes: [],
      page: 1,
      pages: 1,
      address: null,
      type: null,
      connector: null,
      connectors: null
    }
  },
  methods: {
    search: function(event) {
      axios.get(server_api + "/api/nodes", {params: {text: this.search_text, type: this.search_type, offset: (this.page - 1) * this.search_limit, limit: this.search_limit}}).then(response => {
        this.nodes = response.data.results
        this.page = (response.data.offset / response.data.limit) + 1
        this.pages = Math.ceil(response.data.count / response.data.limit)
      })
    },
    check_all: function() {
      for(node_index in this.nodes) {
        if(!this.checkbox_all) {
          this.selected_nodes.push(this.nodes[node_index].uid)
        } else {
          var index = this.selected_nodes.indexOf(this.nodes[node_index].uid)
          if(index > -1) {
            this.selected_nodes.splice(index, 1)
          }
        }
      }
    },
    add: function() {
      axios.put(server_api + "/api/nodes/add", {address: this.address, type: this.type, connector: this.connector}).then(response => {
        $("#add_node").modal("hide")

        router.push({name: "node", params: {uid: response.data.uid}})
      }).catch(error => {
        console.log("lol")
        console.log(error.response.data)
      })
    },
    remove: function() {
      for(var node_index in this.selected_nodes) {
        axios.delete(server_api + "/api/node/" + this.selected_nodes[node_index])
      }

      this.checkbox_all = false

      this.search()
    }
  },
  watch: {
    page: function(val) {
      this.search()
    }
  },
  filters: {
    type_to_str: function(value, node_types) {
      for (var type in node_types) {
        if (value == node_types[type].id) {
          return node_types[type].name
        }
      }

      return "Unknown"
    }
  },
  mounted() {
    axios.get(server_api + "/api/connectors").then(response => {
      this.connectors = response.data
    })

    this.search()
  }
}
