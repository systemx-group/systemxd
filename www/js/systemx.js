const server_api = "http://localhost:8080"

const node_routes = {
  path: "/node/:uid",
  redirect: { name: "node_summary" },
  name: "node",
  components: {
    navbar: navbar,
    default: node,
  },
  children: [
    {
      path: "summary",
      name: "node_summary",
      component: node_summary
    },
    {
      path: "hardwares",
      name: "node_hardwares",
      component: node_hardwares
    },
    {
      path: "softwares",
      name: "node_softwares",
      component: node_softwares
    },
    {
      path: "networks",
      name: "node_networks",
      component: node_networks
    },
    {
      path: "smbios",
      name: "node_smbios",
      component: node_smbios
    },
    {
      path: "ipmi",
      name: "node_ipmi",
      component: node_ipmi
    },
    {
      path: "settings",
      name: "node_settings",
      component: node_settings
    },
    {
      path: "logs",
      name: "node_logs",
      component: node_logs
    }
  ]
}

const application_routes = {
  path: "/application",
  components: {
    navbar: navbar,
    default: application
  },
  children: []
}

const routes = [
  {
    path: "/",
    name: "home",
    components: {
      navbar: navbar,
      default: home
    }
  },
  {
    path: "/nodes",
    name: "nodes",
    components: {
      navbar: navbar,
      default: nodes
    }
  },
  node_routes,
  application_routes,
  {
    path: "/settings",
    name: "settings",
    components: {
      navbar: navbar,
      default: settings
    }
  },
  {
    path: "/plugins",
    name: "plugins",
    components: {
      navbar: navbar,
      default: plugins
    }
  },
  {
    path: "*",
    name: "not_found",
    components: {
      navbar: navbar,
      default: not_found
    }
  }
]

const router = new VueRouter({
  mode: "history",
  routes: routes
})

var node_plugin_tabs = []
var applications = []

const SystemX = {
  loadPlugin(name) {
    var script = document.createElement("script")
    script.onload = function() {
      console.log("Load " + name + " plugin !")
    }
    script.onerror = function() {
      console.log("Failed to load " + name + " plugin !")
    }
    script.src = "/js/plugins/" + name + ".js"

    document.head.appendChild(script)
  },
  register(plugin, data) {
    for (const i in data.node_tabs) {
      const node_tab = data.node_tabs[i]
      node_plugin_tabs.push({
        name: node_tab.name,
        alias: node_tab.alias,
        connector: (node_tab.connector_only) ? plugin : null
      })

      var node_route = routes.find(r => r.path === node_routes.path)
      node_route.children = [{
        path: node_route.path + "/" + plugin + "/" + node_tab.alias,
        name: node_tab.alias,
        component: node_tab.component
      }]

      router.addRoutes([node_route])
    }

    if(data.application) {
      applications.push({
        name: data.application.name,
        alias: plugin
      })

      var application_route = routes.find(r => r.path === application_routes.path)
      application_route.children = [{
        path: application_route.path + "/" + plugin,
        name: plugin,
        component: data.application.component
      }]

      router.addRoutes([application_route])
    }
  }
}

SystemX.loadPlugin("livebox")
SystemX.loadPlugin("virtualization")
SystemX.loadPlugin("deployment")

const app = new Vue({
  router,
  data: {
    node_plugin_tabs: node_plugin_tabs,
    applications: applications
  }
}).$mount("#app")
