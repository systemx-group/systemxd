const navbar = {
  template: `<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">SystemX</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <router-link class="nav-link" exact-active-class="active" :to="{name: 'home'}">Home <span class="sr-only">(current)</span></router-link>
      </li>
      <li class="nav-item">
        <router-link class="nav-link" exact-active-class="active" :to="{name: 'nodes'}">Nodes</router-link>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown">
          Applications
        </a>
        <div class="dropdown-menu">
          <router-link class="dropdown-item" exact-active-class="active" v-for="(application, index) in this.$parent.$data.applications" :key="index" :to="{ name: application.alias }">
            {{ application.name }}
          </router-link>
        </div>
      </li>
      <li class="nav-item">
        <router-link class="nav-link" exact-active-class="active" :to="{name: 'plugins'}">Plugins</router-link>
      </li>
      <li class="nav-item">
        <router-link class="nav-link" exact-active-class="active" :to="{name: 'settings'}">Settings</router-link>
      </li>
    </ul>
  </div>
</nav>`
}
